<?php
/**
 * Created by PhpStorm.
 * User: zp
 * Date: 11.12.2016
 * Time: 01:10
 */

namespace App;

use Slim\App;
use Slim\Views\PhpRenderer;

class SlimApp extends App
{
    /** @var  App */
    private static $instance;

    public static function getInstance()
    {
        if (!self::$instance instanceof SlimApp)
        {
            self::$instance = new SlimApp(["settings" => ["displayErrorDetails" => true]]);
        }

        return self::$instance;
    }

    public function getView()
    {
        return new PhpRenderer('../templates/');
    }

    public function getViewWeb()
    {
        return new PhpRenderer('../public/templates/');
    }
}