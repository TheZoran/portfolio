<?php
/**
 * Created by PhpStorm.
 * User: zp
 * Date: 11.12.2016
 * Time: 04:22
 */

namespace App\Middleware;


use App\Routes;
use Slim\Http\Request;
use Slim\Http\Response;

class AccountAccess
{
    public function __invoke(Request $request, Response $response, callable $next)
    {
        if (!\Account::hasSession())
        {
            return $response = $response->withRedirect(Routes::ACCESS_DENIED);
        }

        $account = \Account::getSession();

        if (!$account->isLoggedIn())
        {
            return $response = $response->withRedirect(Routes::ACCESS_DENIED);
        }

        return $response = $next($request, $response);
    }
}