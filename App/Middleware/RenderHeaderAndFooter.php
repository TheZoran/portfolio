<?php
/**
 * Created by PhpStorm.
 * User: zp
 * Date: 11.12.2016
 * Time: 04:44
 */

namespace App\Middleware;


use App\SlimApp;
use Slim\Http\Request;
use Slim\Http\Response;

class RenderHeaderAndFooter extends BaseMiddleware
{
    public function __invoke(Request $request, Response $response, callable $next)
    {
        $response = $this->render($response, 'header.php');
        $response = $this->render($response, 'navigation.php');
        $response = $next($request, $response);
        $response = $this->render($response, 'footer.php');

        return $response;
    }
}
