<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 15.12.16
 * Time: 13:43
 */

namespace App\Middleware;

use App\SlimApp;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Slim\Http\Response;

abstract class BaseMiddleware
{
    public abstract function __invoke(Request $request, Response $response, callable $next);

    protected function render(ResponseInterface $response, string $template, array $data = [])
    {
        return SlimApp::getInstance()->getView()->render($response, $template, $data);
    }

    protected function renderWeb(ResponseInterface $response, string $template, array $data = [])
    {
        return SlimApp::getInstance()->getViewWeb()->render($response, $template, $data);
    }
}