<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 15.12.16
 * Time: 13:42
 */

namespace App\Middleware;


use App\Routes;
use Slim\Http\Request;
use Slim\Http\Response;

class AccountTagAccess extends BaseMiddleware
{
    public function __invoke(Request $request, Response $response, callable $next)
    {
        if (!\Account::hasSession())
        {
            return $response = $response->withRedirect(Routes::ACCESS_DENIED);
        }

        $account = \Account::getSession();

        if (!$account->isTagAccepted())
        {
            return $response = $response->withRedirect(Routes::ACCESS_DENIED);
        }

        return $response = $next($request, $response);
    }
}