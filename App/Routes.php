<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 17.12.16
 * Time: 01:13
 */

namespace App;


class Routes
{
    const ACCESS_DENIED = "/kommsthiernetrein";
    const CONTACT = "/contact";
    const HOME = "/home";
    const VITA = "/vita";
    const LOGIN = "/login";
    const LOGIN_WITH_USERNAME = "/login/{username}";
    const LOGOUT = "/logout";
}