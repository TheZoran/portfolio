<?php
/**
 * Created by PhpStorm.
 * User: zp
 * Date: 11.12.2016
 * Time: 03:09
 */

namespace App\Controller;


use Slim\Http\Request;
use Slim\Http\Response;

class RequirePassPhrase extends Controller
{
    public function __invoke(Request $request, Response $response, array $args)
    {
        $query = \AccountQuery::create()
            ->filterByTag($args['guest_tag']);

        $guestAccount = $query->findOne();

        if (!$guestAccount instanceof \Account)
        {
            return $response->withRedirect("kommsthiernetrein");
        }

        $guestAccount->setTagAccepted(true);

        \Account::setSession($guestAccount);

        return $this->getView()->render($response, 'askpassphrase.php', ['guestAccount' => $guestAccount]);
    }
}
