<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 17.12.16
 * Time: 01:32
 */

namespace App\Controller;

use Slim\Http\Request;
use Slim\Http\Response;

class Games extends Controller
{
    public function rohnJambo(Request $request, Response $response, array $args)
    {
        return $response = $this->getViewWeb()->render($response, '../games/rj/index.html');
    }
}