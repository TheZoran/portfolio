<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 19.12.16
 * Time: 00:01
 */

namespace App\Controller;

use Slim\Http\Request;
use Slim\Http\Response;

class NavigationItemApi extends Controller
{
    public function getAccountSessionNavigationItems(Request $request, Response $response, array $args)
    {
        if (!\Account::hasSession())
        {
            return $response;
        }

        $itemsJson = \Account::getSession()->getNavigationItems()->toJSON();

        return $response->getBody()->write($itemsJson);
    }

    public function addNavigationItemToAccount(Request $request, Response $response, array $args)
    {
        $item_id    = $request->getParam('item_id');
        $account_id = $request->getParam('account_id');

        $navigationItem = \NavigationItemQuery::create()->findPk($item_id);
        $account        = \AccountQuery::create()->findPk($account_id);

        if ($navigationItem instanceof \NavigationItem && $account instanceof \Account)
        {
            $a2ni = \AccountToNavigationItemQuery::create()
                ->filterByAccount($account)
                ->filterByNavigationItem($navigationItem)
                ->findOneOrCreate();

            if ($a2ni->isNew())
            {
                $a2ni->save();
            }
        }

        return $response;
    }

    public function getNavigationItems(Request $request, Response $response, array $args)
    {
        $items_json = \NavigationItemQuery::create()->find()->toJSON();

        return $response->getBody()->write($items_json);
    }

    public function removeNavigationItemFromAccount(Request $request, Response $response, array $args)
    {
        $item_id    = $request->getParam('item_id');
        $account_id = $request->getParam('account_id');

        $navigationItem = \NavigationItemQuery::create()->findPk($item_id);
        $account        = \AccountQuery::create()->findPk($account_id);

        if ($navigationItem instanceof \NavigationItem && $account instanceof \Account)
        {
            $a2ni = \AccountToNavigationItemQuery::create()
                ->filterByAccount($account)
                ->filterByNavigationItem($navigationItem)
                ->findOne();

            if ($a2ni instanceof \Account)
            {
                $a2ni->delete();
            }
        }

        return $response;
    }
}
