<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 17.12.16
 * Time: 02:49
 */

namespace App\Controller;

use Slim\Http\Request;
use Slim\Http\Response;

class Contact extends Controller
{
    public function __invoke(Request $request, Response $response, array $args)
    {
        return $response = $this->getViewWeb()->render($response, 'contact.html');
    }
}