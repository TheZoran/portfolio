<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 15.12.16
 * Time: 11:46
 */

namespace App\Controller;

use Slim\Http\Request;
use Slim\Http\Response;

class AccountApi extends Controller
{
    public function getInfo(Request $request, Response $response, array $args)
    {
        if (!\Account::hasSession())
        {
            return $response;
        }

        $guestAccount = \Account::getSession();

        $info = new \stdClass();
        $info->name = $guestAccount->getName();

        $infoJson = json_encode($info);

        return $response->getBody()->write($infoJson);
    }

    public function getAccounts(Request $request, Response $response, array $args)
    {
        $accountsJson = \AccountQuery::create()->find()->toJSON();

        return $response->getBody()->write($accountsJson);
    }
}
