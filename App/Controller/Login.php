<?php
/**
 * Created by PhpStorm.
 * User: zp
 * Date: 11.12.2016
 * Time: 03:29
 */

namespace App\Controller;


use App\Routes;
use Slim\Http\Request;
use Slim\Http\Response;

class Login extends Controller
{
    public function checkTag(Request $request, Response $response, array $args)
    {

        $guestAccount = \AccountQuery::create()
            ->filterByTag($args['guest_tag'])
            ->findOne();

        if (!$guestAccount instanceof \Account)
        {
            return $response->withRedirect(Routes::ACCESS_DENIED);
        }

        \Account::setSession($guestAccount);

        $guestAccount = \Account::getSession();
        $guestAccount->setTagAccepted(true);

        return $response = $this->getView()->render($response, 'askpassphrase.php', ['guestAccount' => $guestAccount]);
    }

    public function askForPassphrase(Request $request, Response $response, array $args)
    {
        // If there is no Session, it means he didn't entered the Page with a guilty Guest-Tag
        if (!\Account::hasSession())
        {
            return $response->withRedirect(Routes::ACCESS_DENIED);
        }

        $guestAccount = \Account::getSession();

        if ($request->getParam('passphrase') !== $guestAccount->getAccessPhrase())
        {
            return $response->withRedirect(Routes::ACCESS_DENIED);
        }

        $guestAccount->setPassPhraseAccepted(true);

        return $response = $response->withRedirect('home');
    }

    public function defaultLogin(Request $request, Response $response, array $args)
    {
        if (\Account::hasSession() && \Account::getSession()->isLoggedIn())
        {
            return $response->withRedirect(Routes::HOME);
        }

        $username = '';
        if (isset($args['username']))
        {
            $username = $args['username'];
        }

        if ($request->getParam('username') !== null)
        {
            $username = $request->getParam('username');
        }

        if ($request->getParam('password') !== null)
        {
            return $this->performLogin($request, $response, $args);
        }

        return $response = $this->getView()->render($response, 'login.php', ['username' => $username]);
    }

    public function performLogin(Request $request, Response $response, array $args)
    {
        $username = $request->getParam('username');
        $password = $request->getParam('password');

        $account = \AccountQuery::create()
            ->filterByName($username)
            ->filterByPassword($password)
            ->findOne();

        if (!$account instanceof \Account)
        {
            return $response = $this->getView()->render($response, 'login.php', ['username' => $username, 'login_error' => true]);
        }

        $account->setLoggedIn(true);

        \Account::setSession($account);

        return $response->withRedirect(Routes::HOME);
    }

    public function logout(Request $request, Response $response, array $args)
    {
        \Account::unsetSession();
        return $response->withRedirect(Routes::LOGIN);
    }
}
