<?php
/**
 * Created by PhpStorm.
 * User: zp
 * Date: 11.12.2016
 * Time: 02:14
 */

namespace App\Controller;


use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use App\SlimApp;
use Slim\Route;
use Interop\Container\ContainerInterface;
use Slim\Views\PhpRenderer;

abstract class Controller
{
    private $ci;

    public function __construct(ContainerInterface $ci)
    {
        $this->ci = $ci;
    }

    public function getContainer(string $id)
    {
        if ($this->ci->has($id))
        {
            return $this->ci->get($id);
        }

        return null;
    }

    protected function getApp()
    {
        return SlimApp::getInstance();
    }

    protected function getView(): PhpRenderer
    {
        return $this->getApp()->getView();
    }

    protected function getViewWeb(): PhpRenderer
    {
        return $this->getApp()->getViewWeb();
    }

    protected function render(ResponseInterface $response, string $template, array $data = []): ResponseInterface
    {
        return $this->getView()->render($response, $template, $data);
    }

    protected function renderWeb(ResponseInterface $response, string $template, array $data = []): ResponseInterface
    {
        return $this->getViewWeb()->render($response, $template, $data);
    }
}