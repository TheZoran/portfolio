<?php
/**
 * Created by PhpStorm.
 * User: zp
 * Date: 11.12.2016
 * Time: 04:08
 */

namespace App\Controller;


use Slim\Http\Request;
use Slim\Http\Response;

class Home extends Controller
{
    public function __invoke(Request $request, Response $response, array $args)
    {
        return $response = $this->getView()->render($response, 'home.php');
    }
}