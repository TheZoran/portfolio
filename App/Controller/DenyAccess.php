<?php
/**
 * Created by PhpStorm.
 * User: zp
 * Date: 11.12.2016
 * Time: 02:17
 */

namespace App\Controller;


use Slim\Http\Request;
use Slim\Http\Response;

class DenyAccess extends Controller
{
    public function __invoke(Request $request, Response $response, array $args)
    {
        return $this->getViewWeb()->render($response, "kommsthiernetrein.html");
    }
}