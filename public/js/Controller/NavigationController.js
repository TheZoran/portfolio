var NavigationController = new Vue({
    el: "#navigation",
    data: {
        items: []
    },
    methods: {
        setNavigationItems: function (items) {
            this.items = items;
        }
    }
});

var items = [
    { name: "Home",        icon: "home",        link: "/home"},
    { name: "Werdegang",   icon: "description", link: "/vita"},
    { name: "Fähigkeiten", icon: "gavel",       link: "/skills"},
];

NavigationController.setNavigationItems(items);