var AccountController = new Vue({
    el: "account",
    data: {
        account: null
    },
    methods: {
        getAccount: function () {
            AccountApiCommunicator.getAccountData(this.setAccount)
        },
        setAccount: function (account) {
            this.account = account;
        }
    }
});