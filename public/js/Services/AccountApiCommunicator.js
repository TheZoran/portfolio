var AccountApiCommunicator = {
    getAccountData: function (callback_function) {
        var accountData = window.localStorage.getItem("account_data");
        if (accountData) {
            callback_function(accountData);
            return;
        }

        $.get('api/account/get/session')
            .success(function (data) {
                window.localStorage.setItem("account_data", data);
                callback_function(data);
            });
    }
};
