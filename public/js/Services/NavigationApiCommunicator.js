var NavigationApiCommunicator = {
    getNavigationItems: function (callback_function) {
        var items = window.localStorage.getItem("navigation_items");
        if (items) {
            callback_function(items);
            return;
        }

        Vue.http.get('api/navigation/account_items')
            .then(function () {
                var items = [];
                window.localStorage.setItem("navigation_items", items);
                callback_function(items);
            });
    }
};
