<div class="demo-drawer mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
    <header id="account" class="demo-drawer-header">
        <img src="images/buddy.jpg" style="width: 100%; height: 100%;">
    </header>
    <nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800" id="navigation">
        <a class="mdl-navigation__link" v-bind:href="item.link" v-for="item in items" style="cursor: pointer">
            <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">
                {{ item.icon }}
            </i>
            {{ item.name }}
        </a>
        <div class="mdl-layout-spacer"></div>
        <a class="mdl-navigation__link" href=""><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">help_outline</i><span class="visuallyhidden">Help</span></a>
    </nav>
</div>
<script type="text/javascript" src="js/Controller/NavigationController.js"></script>