<?php /** @var string $username */?>
<?php $login_error = isset($login_error) && $login_error ?>
<?php $password_auto_focus = $username !== "" ? "autofocus" : "" ?>
<?php $username_auto_focus = $username === "" ? "autofocus" : "" ?>

<html>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.cyan-light_blue.min.css">
<link rel="stylesheet" href="mdl/styles.css">
<style>
    .mdl-layout {
        align-items: center;
        justify-content: center;
    }
    .mdl-layout__content {
        padding: 24px;
        flex: none;
    }
</style>
</html>
<body>
<div class="mdl-layout mdl-js-layout mdl-color--grey-100">
    <main class="mdl-layout__content">
        <form action="<?= \App\Routes::LOGIN ?>" method="post">
            <div class="mdl-card mdl-shadow--6dp">
                <div class="mdl-card__title mdl-color--primary mdl-color-text--white">
                    <h2 class="mdl-card__title-text">Login</h2>
                </div>
                <div class="mdl-card__supporting-text">
                    <?php if ($login_error): ?>
                    <p style="color: red">
                        Username oder Passwort falsch.
                    </p>
                    <?php endif; ?>
                    <form action="#">
                        <div class="mdl-textfield mdl-js-textfield">
                            <input class="mdl-textfield__input" type="text" id="username" name="username" placeholder="Username" value="<?= $username ?>" <?= $username_auto_focus ?> />
                            <label class="mdl-textfield__label" for="username"></label>
                        </div>
                        <div class="mdl-textfield mdl-js-textfield">
                            <input class="mdl-textfield__input" type="password" id="password" name="password" placeholder="Passwort" <?= $password_auto_focus ?> />
                            <label class="mdl-textfield__label" for="userpass"></label>
                        </div>
                    </form>
                </div>
                <div class="mdl-card__actions mdl-card--border">
                    <button class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">Log in</button>
                </div>
            </div>
        </form>
    </main>
</div>
</body>