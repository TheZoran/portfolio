<?php
/**
 * Created by PhpStorm.
 * User: zp
 * Date: 30.11.2016
 * Time: 20:39
 */

namespace App\Events;


abstract class EventHandler
{
    abstract public function handle($event);
    abstract public function canHandle($event): bool;
}