<?php

namespace App\Events;


class LoginFailedHandler extends EventHandler
{
    /**
     * @param $event LoginFailed
     */
    public function handle($event)
    {

    }

    public function canHandle($event): bool
    {
        return $event instanceof LoginFailed;
    }
}