<?php

namespace App\Events;

class LoginFailed extends Event
{
    private $httpRequest;
    private $username;
    private $password;

    /**
     * LoginFailed constructor.
     * @param string $username
     * @param string $password
     * @param \HttpRequest $httpRequest
     */
    function __construct(string $username, string $password, \HttpRequest $httpRequest)
    {
        $this->username    = $username;
        $this->password    = $password;
        $this->httpRequest = $httpRequest;
    }

    /**
     * @return \HttpRequest
     */
    public function getHttpRequest(): \HttpRequest
    {
        return $this->httpRequest;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}