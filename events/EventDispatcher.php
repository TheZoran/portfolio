<?php

namespace App\Events;


class EventDispatcher
{
    static $instance;
    public static function getInstance(): EventDispatcher
    {
        if (!self::$instance instanceof EventDispatcher)
        {
            self::$instance = new EventDispatcher();
        }

        return self::$instance;
    }

    /** @var EventHandler[] */
    private $handler = [];

    /**
     * @param EventHandler $eventHandler
     */
    public function addEventHandler(EventHandler $eventHandler)
    {
        if (!in_array($eventHandler, $this->handler))
        {
            $this->handler[] = $eventHandler;
        }
    }

    /**
     * @param Event $event
     */
    public function triggerEvent(Event $event)
    {
        foreach ($this->handler AS $handler)
        {
            if ($handler->canHandle($event))
            {
                $handler->handle($event);
            }
        }
    }
}
