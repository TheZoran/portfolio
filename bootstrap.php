<?php

require_once "vendor/autoload.php"; // Autoload

session_start();

require_once "config/propel/config.php"; // Propel Config
require_once "routes/views.php";
require_once "routes/login.php";

use App\SlimApp;
use App\Middleware\DoorManCheck;

$app = SlimApp::getInstance();
$app->run();