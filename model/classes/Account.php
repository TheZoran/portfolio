<?php

use Base\Account as BaseAccount;

/**
 * Skeleton subclass for representing a row from the 'guest_account' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Account extends BaseAccount
{
    /** @var bool */
    private $tagAccepted = false;
    /** @var bool */
    private $passPhraseAccepted = false;
    /** @var bool */
    private $loggedIn = false;

    /**
     * @return bool
     */
    public function isLoggedIn(): bool
    {
        return $this->loggedIn;
    }

    /**
     * @param bool $loggedIn
     */
    public function setLoggedIn(bool $loggedIn)
    {
        $this->loggedIn = $loggedIn;
    }

    /**
     * @return bool
     */
    public function isTagAccepted(): bool
    {
        return $this->tagAccepted;
    }

    /**
     * @param bool $tagAccepted
     */
    public function setTagAccepted(bool $tagAccepted)
    {
        $this->tagAccepted = $tagAccepted;
    }

    /**
     * @return bool
     */
    public function isPassPhraseAccepted(): bool
    {
        return $this->passPhraseAccepted;
    }

    /**
     * @param bool $passPhraseAccepted
     */
    public function setPassPhraseAccepted(bool $passPhraseAccepted)
    {
        $this->passPhraseAccepted = $passPhraseAccepted;
    }

    /** @var string */
    const SESSION_NAME = 'Account';

    /**
     * @return null|Account
     */
    public static function getSession()
    {
        if (!self::hasSession() || !$_SESSION[self::SESSION_NAME] instanceof Account)
        {
            return null;
        }

        return $_SESSION[self::SESSION_NAME];
    }

    /**
     * @param Account $guestAccount
     */
    public static function setSession(Account $guestAccount)
    {
        $_SESSION[self::SESSION_NAME] = $guestAccount;
    }

    /**
     * @return bool
     */
    public static function hasSession(): bool
    {
        return isset($_SESSION[self::SESSION_NAME]) && $_SESSION[self::SESSION_NAME] instanceof Account;
    }

    public static function unsetSession()
    {
        unset($_SESSION[self::SESSION_NAME]);
    }
}
