<?php

use Base\NavigationItemQuery as BaseNavigationItemQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'navigation_item' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class NavigationItemQuery extends BaseNavigationItemQuery
{

}
