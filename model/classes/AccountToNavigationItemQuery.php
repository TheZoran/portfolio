<?php

use Base\AccountToNavigationItemQuery as BaseAccountToNavigationItemQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'account_to_navigation_item' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class AccountToNavigationItemQuery extends BaseAccountToNavigationItemQuery
{

}
