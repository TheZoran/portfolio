<?php

use Slim\Http\Request;
use Slim\Http\Response;
use App\Routes;
use App\Middleware\AccountAccess;
use App\Middleware\RenderHeaderAndFooter;

$app = \App\SlimApp::getInstance();

$app->get(Routes::HOME, 'App\Controller\Home')
    ->add(new AccountAccess())
    ->add(new RenderHeaderAndFooter());

$app->get(Routes::CONTACT, 'App\Controller\Contact')
    ->add(new AccountAccess())
    ->add(new RenderHeaderAndFooter());

$app->get(Routes::VITA, 'App\Controller\Vita')
    ->add(new AccountAccess())
    ->add(new RenderHeaderAndFooter());