<?php
use App\SlimApp;
use App\Middleware\AccountAccess;
use App\Routes;

SlimApp::getInstance()->get(Routes::ACCESS_DENIED, 'App\Controller\DenyAccess');

SlimApp::getInstance()->get(Routes::LOGIN, 'App\Controller\Login:defaultLogin');
SlimApp::getInstance()->post(Routes::LOGIN, 'App\Controller\Login:performLogin');
SlimApp::getInstance()->get(Routes::LOGIN_WITH_USERNAME, 'App\Controller\Login:defaultLogin');
SlimApp::getInstance()->get(Routes::LOGOUT, 'App\Controller\Login:logout');

SlimApp::getInstance()->get('/api/guestaccount/get', 'App\Controller\GuestAccountApi:getInfo')
    ->add(new AccountAccess());